Name:           perl-Text-Template
Version:        1.60
Release:        2
Summary:        Expand template text with embedded Perl
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Text-Template
Source0:        https://cpan.metacpan.org/authors/id/M/MS/MSCHOUT/Text-Template-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  coreutils findutils make perl perl-generators perl(ExtUtils::MakeMaker) perl(Test::More)
Requires:       perl(Carp)

%description
This is a library for generating form letters, building HTML pages, or
filling in templates generally.  A `template' is a piece of text that
has little Perl programs embedded in it here and there.  When you
`fill in' a template, you evaluate the little programs and replace
them with their values.

%package_help

%prep
%autosetup -n Text-Template-%{version} -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%files
%license LICENSE
%{perl_vendorlib}/Text/

%files help
%doc README
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1.60-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.60-1
- Upgrade to version 1.60

* Fri Jan 29 2021 liudabo <liudabo1@huawei.com> - 1.59-1
- upgrade version to 1.59

* Tue Dec 3 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.53-4
- Package init
